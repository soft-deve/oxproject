/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuntapong.tictactoe;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OXProject {
    static int round = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void Input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            //      System.out.println("row: "+ row + "col: "+ col);
            if (table[row][col] == '-') {
                table[row][col] = player;
                round ++;
                break;
            }
            System.out.println("Error: table at row and col not empty!!");
        }

    }
    static void checkCol(){
        for(int row=0; row<3 ;row++){
            if(table[row][col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
     static void checkRow(){
        for(int col=0; col<3 ;col++){
            if(table[row][col] != player){
                return;
            }
        }
        isFinish = true;
        winner = player;
    }
     static void checkX(){
        if(table[0][0] == table[1][1]&& table[1][1] == table[2][2]
                && table[0][0]!='-'){
            isFinish = true;
            winner = table[0][0];
        }
        if(table[0][2] == table[1][1]&& table[1][1] == table[2][0]
                && table[0][2]!='-'){
            isFinish = true;
            winner = table[0][2];
        }
        return;
    }
     static void checkDraw(){
         if(round != 9){
             return;
         }
         isFinish = true;
     }
    static void CheckWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }
    
    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
       if(winner == '-'){
           System.out.println("Draw!!");
       }else{
           System.out.println(winner + " win!!");
       }
    }

    static void showBye() {
        System.out.println("Bye bye ...");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            Input();
            CheckWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();

    }
}
